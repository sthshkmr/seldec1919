package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateAndView extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateAndView";
		testcaseDec = "Create and View Lead";
		author = "Sathish";
		category = "smoke";
		excelFileName = "TC002";
	} 

	@Test(dataProvider="fetchData") 
	public void createAndView(String uName, String pwd, String cname, String fname, String lname) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickcrm()
		.clickCreateLead()
		.clickCompanyName(cname)
		.clickFirstName(fname)
		.clickLastName(lname)
		.clickCreateLeateButton();
		
		
		
	}}
		