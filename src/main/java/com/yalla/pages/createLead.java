package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class createLead  extends Annotations{

	public createLead() {
	       PageFactory.initElements(driver, this);
	       
}
@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCname;
@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFname;
@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement eleLname;
@FindBy(how=How.CLASS_NAME, using="smallSubmit") WebElement eleCrelead;
public createLead clickCompanyName(String data) {
	//WebElement eleCrlead = locateElement("Text", "Create Lead");
	clearAndType(eleCname, data); 
	return new createLead();
}
public createLead clickFirstName(String data) {
	//WebElement eleCrlead = locateElement("Text", "Create Lead");
	clearAndType(eleFname, data); 
	return new createLead();
}
public createLead clickLastName(String data) {
	//WebElement eleCrlead = locateElement("Text", "Create Lead");
	clearAndType(eleLname, data);
	return new createLead();
}
public viewLead clickCreateLeateButton() {
	//WebElement eleLogin = locateElement("class", "decorativeSubmit");
      click(eleCrelead);  
      return new viewLead();
}}





