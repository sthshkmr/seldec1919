package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class myHome  extends Annotations{

	public myHome() {
	       PageFactory.initElements(driver, this);
	       
}
@FindBy(how=How.LINK_TEXT,using="Create Lead") WebElement eleCrlead;
public createLead clickCreateLead() {
	//WebElement eleCrlead = locateElement("Text", "Create Lead");
	click(eleCrlead);  
	return new createLead();
}}