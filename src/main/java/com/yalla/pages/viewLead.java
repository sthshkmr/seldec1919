package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class viewLead  extends Annotations{

	public viewLead() {
	       PageFactory.initElements(driver, this);
	       
	}
@FindBy(how=How.ID,using="viewLead_firstName_sp") WebElement eleVlead;
public viewLead clickverifytext(String data) {
	verifyExactText(eleVlead, data);
 
	return this;
}}