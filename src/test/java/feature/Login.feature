Feature: Login for leaftap
Scenario: create lead
Given open the browser
And maximize the browser
And load the url
And enter the username as DemoSalesManager
And enter the password as crmsfa
When click the login button
And click the crmsfa button
And click the Create Lead button
And enter the company name as starhooks
And enter the first name as Rathish	
And enter the last name as maddy
And click the submit
Then verify the Create lead successfull



Scenario Outline:: create lead
Given open the browser
And maximize the browser
And load the url
And enter the username as DemoSalesManager
And enter the password as crmsfa
When click the login button
And click the crmsfa button
And click the Create Lead button
And enter the company name as <cname>
And enter the first name as <fname>
And enter the last name as <lname>
And click the submit
Then verify the Create lead successfull

Examples:
|cname|fname|lname|
|TCS|Joe|T|
|HCL|Ram|Kumar|
