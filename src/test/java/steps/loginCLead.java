package steps;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class loginCLead {
	ChromeDriver driver;
	@Given("open the browser")
	public void openTheBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 driver=new ChromeDriver();
	 
	}

	@Given("maximize the browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();
	   
	}

	@Given("load the url")
	public void loadTheUrl() {
		driver.get("http://leaftaps.com/opentaps/control/main");
	 
	}

	@Given("enter the username as (.*)")
	public void enterTheUsernameAsDemoSalesManager(String uname) {
		driver.findElementById("username").sendKeys(uname);
	}

	@Given("enter the password as (.*)")
	public void enterThePasswordAsCrmsfa(String Pwd) {
		driver.findElementById("password").sendKeys(Pwd);
	  
	}

	@When("click the login button")
	public void clickTheLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click(); 
	   
	}

	@When("click the crmsfa button")
	public void clickTheCrmSfaButton() {
		driver.findElementByLinkText("CRM/SFA").click();
	    
	}

	@When("click the Create Lead button")
	public void clickTheCreateLeadButton() {
		driver.findElementByLinkText("Create Lead").click();
	   
	}
	@When("enter the company name as (.*)")
	public void enterTheCompanyName( String cname) {
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
	}

	@When("enter the first name as (.*)")
	public void enterTheFirstName(String fname) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
	}

	@When("enter the last name as(.*)")
	public void enterTheLastName(String lname ) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
	}

	@When("click the submit")
	public void clickTheSubmit() {
	 driver.findElementByClassName("smallSubmit").click();
	}
	
	@Then("verify the Create lead successfull")
	public void verifyTheCreateLeadSuccessfull() {
	   System.out.println("success");
	 
	}
}
